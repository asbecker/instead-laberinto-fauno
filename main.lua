-- $Name:El laberinto del Fauno$
-- $Version: 0.1.0$
-- $Author: Ferroviario K$	
instead_version "1.6.0"

-- Primera prueba de código LUA para el motor INSTEAD.	
-- Módulos requeridos:

require "para" -- parágrafos
require "quotes" -- comillas « y »
require "dash" -- uso de doble guión
require "format" -- formatear texto

-- Codificación del texto:
game.codepage="UTF-8";
game.scene_use = false;
-- Definición de acciones por defecto:
game.act = 'No pasa nada.';
game.inv = 'No puedo llevarme esto.';
game.use = 'No funciona.';
game.forcedsc = true;
-- Inicialización de variables globales.
function init()
end;

var_room_habRosa = vroom('Habitación rosa', 'room_habRosa');
var_room_habCeleste = vroom('Habitación celeste', 'room_habCeleste');
var_room_escPB = vroom('Escaleras a la planta baja', 'room_escPB');
var_room_terraza = vroom('Terraza', 'room_terraza');
    
-- Main

main = room {
	nam = "Introducción",
	dsc = [[Luego de una parranda interminable en Amerika, en la cual 
		has caido desmayado, despiertas en un sitio desconocido para ti. 
		Tienes una jaqueca fenomenal.^^
		Yaces en una cama llena de polvo, húmeda y con extraños puntitos 
		blancos que huelen mal.^^
		Por más que lo intentas, no logras recordar la noche anterior, 
		pero inquietantemente tienes la vaga sensación de que un ser
		híbrido entre Gollum y un fauno te acechaba, y que alguien te 
		alertó sobre él y la burundanga...]],
    way = { 
        vroom('Comenzar la aventura', 'room_habSombria'),
    };
};

-- Rooms 1er piso

room_habSombria = room {
	nam = "Habitación sombría",
	dsc = [[Te encuentras en una habitación sombría, con una cama 
		cuyas sábanas conocieron mejores épocas. 
		Las ventanas están cerradas con madera de cajón de frutas.
		Hay más escombros y tierra que muebles. 
		Un ropero maltrecho está apoyado en la pared que da al este.]],
    way = { 
        vroom('Salir de la habitación', 'room_pasillo1Piso'),
        vroom('Revisar la cama', 'room_camaSombria'),
        vroom('Revisar el ropero', 'room_roperoHabSombria'),
    };
    obj = {
    };
};

room_pasillo1Piso = room {
	nam = "Pasillo del primer piso",
	dsc = [[Estás en un pasillo en forma de T. En la intersección de las 
		tres partes de este pasillo hay una ventana, que mira a la calle. 
		Miras por ella y comprendes que estás en un primer piso. 
		Afuera el sol brilla, pero dentro de esta casa todo es sombrío, 
		y hay humedad en las paredes.]],
    way = { 
        vroom('Volver a la habitación sombría', 'room_habSombria'),
        vroom('Ir a las escaleras', 'room_escalerasTerraza'),
        var_room_escPB:disable(),
        var_room_habCeleste:disable(),
        var_room_habRosa:disable(),
    };
    obj = {
		'obj_HongoParedPasillo1Piso',
		'obj_puertaEscalera1Piso',
		'obj_puertaHabCeleste',
		'obj_puertaHabRosa',
		'obj_rallador', -- lugar temporal ****
    };
};

room_escalerasTerraza = room {
	nam = 'Escaleras a la terraza',
	dsc = [[Estás en unas escaleras cubiertas de tierra y hojas secas de árboles.]], -- ****
	way = {
		vroom('Volver al pasillo del primer piso', 'room_pasillo1Piso'),
		vroom('Entrar a la room_terraza', 'room_terraza'):disable(),
	};
	obj = {
		'obj_rataMuerta', -- agregada a la lista
		'obj_puertaEscTerraza',
	};
};

room_terraza = room {
	nam = 'room_terraza',
	dsc = [[]], -- ****
	way = {
		vroom('Volver a las escaleras al primer piso', 'room_escalerasTerraza'),
	};
};

room_habCeleste = room {
	-- Esta habitación está cerrada con llave.
	-- Aquí está la llave que abre el sótano.
	nam = 'Habitación celeste',
	dsc = [[Estás en una habitación limpia, pintada de celeste claro 
		y con luz natural. Te parece todo un oasis en medio de tanta mugre.^^
		Hay una ventana en la pared que da al sur. Al lado, una cómoda.]],
	way = {
		vroom('Volver al pasillo del primer piso', 'room_pasillo1Piso'),
		vroom('Revisar la cómoda', 'room_comodaHabRosa'),
	};
};

room_habRosa = room {
	nam = 'Habitación rosa',
	dsc = [[Esta habitación está pintada de un rosa chillón, 
		y hay varios posters de hip hop en las paredes. 
		No está del todo limpia pero al menos se nota que aquí barren cada tanto.^^
		]],
	way = {
		vroom('Volver al pasillo del primer piso', 'room_pasillo1Piso'),
	};
	obj = {
		'obj_llaveOxidada', 
	};
};

room_escPB = room {
	nam = 'Escaleras a la planta baja',
	dsc = [[Estás en unas escaleras de cemento a la vista, 
			con manchas de aceite y paredes de ladrillo hueco sin revocar.]],
	way = {
		vroom('Volver al pasillo del primer piso', 'room_pasillo1Piso'),
	};
	obj = {
		'obj_gatoHambriento', -- disposición temporal.
	}
};

room_camaSombria = room {
	nam = 'room_camaSombria',
	disp = 'Cama sucia',
	dsc = 'Miras la cama y sólo ves pendejos, polvo, sábanas manchadas y alguna que otra pulga.',
	obj = {
		'obj_espantasuegras',
		'obj_falopa'
	};
	way = {
		vroom('Dejar de revisar la cama', 'room_habSombria'),
	};
};

room_comodaHabRosa = room {
	nam = 'room_comodaHabRosa',
	disp = 'Cómoda',
	dsc = 'Es una cómoda que tiene cuatro cajones.',
	act = function(s, w)
        if w == "room_comodaHabRosa_cajon3" then  -- ver más abajo
            p [[Este cajón está trabado, y no encuentras el modo de abrirlo.]];
        elseif w == "room_comodaHabRosa_cajon4" then
            p [[Este cajón está trabado, y no encuentras el modo de abrirlo.]];
        end
    end;
	obj = { 
		'obj_virgenMaria',
	   -- objeto lightweight
	   vobj("room_comodaHabRosa_cajon3", "Está trabado."),
	   -- otro lightweight
	   vobj("room_comodaHabRosa_cajon4", "Está trabado."),
     },
	way = {
		vroom('Dejar de revisar la cómoda', 'room_habCeleste'),
		vroom('Revisar el primer cajón', 'room_comodaHabRosaCajon1'),
		vroom('Revisar el segundo cajón', 'room_comodaHabRosaCajon2'),
	};
};

room_comodaHabRosaCajon1 = room {
	nam = 'room_comodaHabRosaCajon1',
	disp = 'Cajón 1',
	dsc = 'Este cajón contiene ropa interior (algunas un poco raidas por lo viejas).',
	obj = {
		'obj_bolitaNaftalina',
		'obj_consolador',
	};
	way = {
		vroom('Dejar de revisar este cajón', 'room_comodaHabRosa'),
	};
};

room_comodaHabRosaCajon2 = room {
	nam = 'room_comodaHabRosaCajon2',
	disp = 'Cajón 2',
	dsc = [[Este cajón contiene papeles de PAMI y recibos de la jubilación. 
		Hay una foto de una señora mayor cuidando a un ser sin pera; te parte el alma.]];
	obj = {
		'obj_plataJubilacion',
		'obj_sobreCacao'
	};
	way = {
		vroom('Dejar de revisar este cajón', 'room_comodaHabRosa'),
	};
};

room_roperoHabSombria = room {
	nam = 'room_roperoHabSombria',
	disp = 'Ropero de la habitación sombría',
	dsc = [[Abres el ropero y encuentras un sinfín de camisas rayadas, 
		pantalones de jean gastados, una mochila con un agujero en el fondo, 
		varios almanaques de Eyeshield 21 y una billetera. Vacía.]];
	obj = {
		'obj_llavePlateada',
	};
	way = {
		vroom('Dejar de revisar el ropero', 'room_habSombria'),
	};
};

-- Rooms planta baja

-- Room sótano

-- Room room_terraza


-- Objetos no recogibles

obj_HongoParedPasillo1Piso = obj {
	nam = 'obj_HongoParedPasillo1Piso',
	disp = 'Hongo de pared',
	dsc = 'En una pared ves un {hongo} más grande que los demás.',
	act = [[Además de ensuciarte las manos y enchastrar aun más la pared, no pasa nada.]],
};

obj_puertaEscalera1Piso = obj {
	-- A esta escalera se accede con la llave plateada.
	nam = 'obj_puertaEscalera1Piso',
	disp = 'Puerta de la escalera del primer piso',
	dsc = [[Doblando el recodo a la izquierda y al final del pasillo, 
		una {puerta, de madera raída}. En el extremo opuesto de este pasillo, 
		unas escaleras que suben.]],
	var = { abierta = false };
	act = function(s, w)
        if s.abierta == true then
            p [[La puerta está abierta.]];
            var_room_habCeleste:enable()
        else
            p [[No tienes la llave apropiada]];
        end
    end;
};

obj_puertaHabCeleste = obj {
	-- A esta habitación se accede con la llave oxidada.
	nam = 'obj_puertaHabCeleste',
	disp = 'Puerta blanca',
	dsc = [[Tras de ti tienes la puerta abierta de la habitación sombría. 
		Frente a ella hay otra {puerta, pintada de blanco}.]],
	var = { abierta = false };
	act = function(s, w)
        if s.abierta == true then
            p [[La cerradura está abierta.]];
        else
            p [[No tienes la llave apropiada]];
        end
    end;
};

obj_puertaHabRosa = obj {
	-- A esta habitación se puede acceder sin llave.
	nam = 'obj_puertaHabRosa',
	disp = 'Puerta de la habitación azul',
	dsc = 'Perpendicularmente a ésta, tienes aun {otra puerta, pintada de rosa}.',
	act = function(s)
        var_room_habRosa:enable()
        p [[¡Logras abrir la puerta rosa!]];
    end,
};

obj_puertaEscTerraza = obj {
	-- A esta habitación se accede con la llave Yale dorada.
	nam = 'obj_puertaEscTerraza',
	disp = 'Puerta de la room_terraza',
	dsc = 'Arriba, en lo alto de los peldaños, hay una {puerta de chapa} pintada de negro.',
	var = { abierta = false };
	act = function(s, w)
        if s.abierta == true then
            p [[La cerradura está abierta.]]
        else
            p [[No tienes la llave apropiada]];
        end
    end;
};

obj_virgenMaria = obj {
	nam = 'obj_virgenMaria',
	disp = 'Estatuilla de la Virgen María',
	dsc = 'Arriba de la cómoda hay una {estatua de la Virgen María}.',
	act = 'No es momento de rezar, campeón.',
}

obj_gatoHambriento = obj {
	nam = 'obj_gatoHambriento',
	disp = 'Gato hambriento', 
	dsc = 'Hay un {gato} raquítico pidiendo comida con maullidos lastimeros.',
	act = [[¡Meow meowdamecomida meow!]];
};

-- Objetos recogibles

obj_llaveOxidada = obj { 
	-- Esta llave abre la puerta de la habitación matrimonial, o sea
	-- obj_puertaHabCeleste
    nam = 'obj_llaveOxidada',
    disp = 'Llave oxidada',
    dsc = 'También encuentras una {llave oxidada}, al parecer muy vieja.',  
    act = 'Abres la puerta.',
    use = function(s, w)
        if w ~= obj_puertaHabCeleste then
            p [[No abre esta cerradura.]]
            return false
        else
            p 'Logras abrir la cerradura de la puerta blanca.';
            w.abierta = true
            var_room_habCeleste:enable()
        end
    end;
    tak = 'Agarras la llave oxidada',
};

obj_llaveDorada = obj { 
	-- Esta llave abre la puerta de la terraza.
    nam = 'obj_llaveDorada',
    disp = 'Llave dorada',
    dsc = 'Al lado del cadáver del gato encuentras una {llave dorada}.',  
    act = 'Abres la puerta.',
    use = function(s, w)
        if w ~= obj_puertaEscTerraza then
            p [[No abre esta cerradura.]]
            return false
        else
            p 'Logras abrir la cerradura de la puerta a la terraza.';
            w.abierta = true
            var_room_terraza:enable()
        end
    end;
    tak = 'Agarras la llave dorada',
};

obj_llavePlateada = obj { 
	-- Esta llave abre la puerta de la escalera a la planta baja, o sea
	-- puerta_room_escPB
    nam = 'obj_llavePlateada',
    disp = 'Llave plateada',
    dsc = 'También encuentras una {llave plateada}, al parecer muy vieja.',
    act = 'Abres la puerta.',
    use = function(s, w)
        if w ~= obj_puertaEscalera1Piso then
            p [[No abre esta cerradura.]]
            return false
        else
            p 'Logras abrir la puerta de madera.';
            w.abierta = true
            var_room_escPB:enable()
        end
    end;
    tak = 'Agarras la llave plateada',
};

obj_espantasuegras = obj { 
    nam = 'obj_espantasuegras',
    disp = 'Espantasuegras',
    dsc = 'Tanteas la cama y encuentras un {espantasuegras} de la noche anterior.',  
    act = 'Usas el espantasuegras.',
    tak = [[Agarras el espantasuegras.]];
};

obj_falopa = obj { 
    nam = 'obj_falopa',
    disp = 'Bolsa de marihuana',
    dsc = 'También encuentras una {bolsa de marihuana}, ¡y son flores!',
    act = 'Abres la puerta.',
    tak = [[Agarras la bolsa de marihuana.]];
};

obj_rataMuerta = obj { 
    nam = 'obj_rataMuerta',
    disp = 'Rata muerta',
    dsc = 'Una {rata muerta} yace tirada en el descanso de la escalera.',
    act = 'Está largando un olor espantoso.',
    use = function(s, w)
        if w == obj_gatoHambriento then
			p [[¡El gato devora la rata muerta con celeridad!^^
				En gratitud te entrega una llave dorada]];
			take(obj_llaveDorada)
			remove(s, me())
			return true
        elseif w == obj_virgenMaria then
			p [[¡Eres un hereje!]];
			return false
        else
			p [[¡Guarda eso, larga un olor espantoso!]];
            return false
        end
    end;
    tak = 'Con muchísimo asco agarras la rata.',
};

obj_bolitaNaftalina = obj {
    nam = 'obj_bolitaNaftalina',
    disp = 'Bolita de naftalina',
    dsc = 'Varias bolitas de naftalina gastadas, excepto {una que está entera}.',
    tak = 'Agarras la bolita de naftalina (qué olor a viejo).',
};
obj_consolador = obj {
	nam = 'obj_consolador',
	disp = 'Consolador',
	dsc = [[Al fondo del cajón encuentras un {viejo consolador}, 
		ya gastado y al parecer sin lavar.]],
	tak = [[Guardas el consolador en uno de tus bolsillos. 
		Pareces muy feliz, pero créeme, no lo eres.]],
};
obj_plataJubilacion = obj {
	nam = 'obj_plataJubilacion',
	disp = 'La jubilación de la madre',
	dsc = 'Entre las bombachas ves varios {billetes de 50 pesos} hechos un bollito.',
	tak = 'Te robas la jubilación de la pobre anciana.',
};
obj_sobreCacao = obj {
	nam = 'obj_sobreCacao',
	disp = 'Sobre de Nesquik',
	dsc = 'Al fondo de este cajón ves un {sobre de cacao}.',
	tak = 'Agarras el sobre de cacao.',
};

obj_natalinaRallada = obj { 
	nam = 'obj_natalinaRallada',
	disp = 'Naftalina rallada',
	tak = 'Obtienes naftalina rallada.',
};

obj_rallador = obj { 
	nam = 'obj_rallador',
	disp = 'Rallador de cocina',
	dsc = 'Hay un {rallador de cocina}, algo oxidado.',
    use = function(s, w)
        if w == obj_bolitaNaftalina then
            take(obj_natalinaRallada);
			inv():del(w);
			p [[Rallas la naftalina y obtienes naftalina rallada, 
				un explosivo. Eres todo un anarquista tirabombas.]];
        elseif w == obj_rataMuerta then
			p [[¡Cómo vas a rallar una rata muerta, animal!]];
			return false
		elseif w == obj_falopa then
			p [[¡La marihuana ya está picada, falopero!]];
			return false
		elseif w == obj_sobreCacao then
			p [[Si rallaras eso, harías un enchastre peor que el de esta casa.]];
			return false
        else
			p [[No puedes rallar eso, campeón.]];
            return false
        end
    end;
    tak = 'Agarras el rallador',
};
