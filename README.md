# INSTEAD-laberinto-fauno
Laberinto del fauno - juego para el motor INSTEAD

Este repositorio contiene los scripts Lua de un juego para el motor
INSTEAD.

El juego es una aventura en modo texto, en el cual el personaje debe
huir de una casa donde vive un ser malvado y sin mentón.

TODO ESTE SOFTWARE SE BRINDA "TAL CUAL", SIN GARANTÍAS DE NINGÚN TIPO.
Cualquier situación derivada del uso del mismo no será responsabilidad
del desarrollador ni de BitBucket; toda la responsabilidad por el uso o
modificación del código o de los archivos binarios quedará bajo la 
exclusiva responsabilidad del usuario.

Para más información, sírvase leer [la licencia](https://github.com/arielsbecker/INSTEAD-laberinto-fauno/blob/master/LICENSE).
INSTEAD es un motor para juegos de rol, se descarga [haciendo clic aquí](http://instead.syscall.ru/download/).
